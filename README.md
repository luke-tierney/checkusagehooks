# Package `checkUsageHooks`

This packages provides two functions to enable checking of function
definitions as they are added to an R session:

- `addCheckUsageCallback` uses `addTaskCallback` to register a
  callback that checks the value of each top level assignment of a
  `function` expression to a symbol.

- `enableSourceCheckUsage` anables or disables `trace` on `source` to
  check the value of each top level assignment of a `function`
  expression to a symbol.
  
The arguments for controlling which checks to report can be set with
the `checkUsageHookOptions` option. The default is `all = TRUE`.
